import boto3
from pytest import mark

class HellowYelpTests:

    def test_athena_query(self):
        athena = boto3.client('athena',region_name="us-east-1")
        results = athena.start_query_execution(
            QueryString = 'select * from business',
            QueryExecutionContext = {
                'Database':'dev'
            },
            ResultConfiguration = {
                'OutputLocation': 's3://jj-query-bucket/'
            }
        )

        assert results



