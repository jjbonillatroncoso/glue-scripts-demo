print('glue job successful')

from awsglue.utils import getResolvedOptions
import sys
from dummy_package.dummy_class import DummyClass

args = getResolvedOptions(sys.argv, [
    "param1","param2"
    ])

print(args)
print(DummyClass())