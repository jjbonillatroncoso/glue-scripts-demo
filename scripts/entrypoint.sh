#!/bin/sh

set -e
cd /home/libraries/custom_library
python setup.py bdist_wheel
pip install dist/dummy_package-0.1-py3-none-any.whl
aws s3 cp /home/libraries/custom_library/dist/dummy_package-0.1-py3-none-any.whl s3://jj-datalake/libraries/dummy_package-0.1-py3-none-any.whl
cd /home/jobs/yelp_scripts
python hellow_yelp.py --param1 demo1 --param2 demo2