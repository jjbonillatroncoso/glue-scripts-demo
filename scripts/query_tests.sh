#!/bin/sh

set -e
cd /home/libraries/custom_library
python setup.py bdist_wheel
aws s3 cp /home/libraries/custom_library/dist/dummy_package-0.1-py3-none-any.whl s3://jj-datalake/libraries/dummy_package-0.1-py3-none-any.whl
cd /home/jobs
pytest