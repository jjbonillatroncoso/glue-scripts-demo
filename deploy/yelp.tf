resource "aws_glue_catalog_table" "aws_glue_catalog_table" {
  name          = "business"
  database_name = "yelp"

  table_type = "EXTERNAL_TABLE"

  parameters = {
    EXTERNAL              = "TRUE"
    "parquet.compression" = "SNAPPY"
  }

  storage_descriptor {
    location      = "s3://jj-powerbi-docs/yelp_data/business"
    input_format  = "org.apache.hadoop.mapred.TextInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"

    ser_de_info {
      name                  = "default"
      serialization_library = "org.openx.data.jsonserde.JsonSerDe"
      parameters = {
        "paths" = "address,attributes,business_id,categories,city,hours,is_open,latitude,longitude,name,postal_code,review_count,stars,state"
      }
    }


    columns {
      name = "business_id"
      type = "string"
    }

    columns {
      name = "name"
      type = "string"

    }
    columns {
      name = "address"
      type = "string"

    }
    columns {
      name = "city"
      type = "string"

    }
    columns {
      name = "state"
      type = "string"

    }
    columns {
      name = "postal_code"
      type = "string"

    }
    columns {
      name = "latitude"
      type = "double"

    }
    columns {
      name = "longitude"
      type = "double"

    }
    columns {
      name = "stars"
      type = "double"
    }

    columns {
      name = "review_count"
      type = "int"

    }
    columns {
      name = "is_open"
      type = "int"

    }
    columns {
      name = "attributes"
      type = "struct<RestaurantsTableService:string,WiFi:string,BikeParking:string,BusinessParking:string,BusinessAcceptsCreditCards:string,RestaurantsReservations:string,WheelchairAccessible:string,Caters:string,OutdoorSeating:string,RestaurantsGoodForGroups:string,HappyHour:string,BusinessAcceptsBitcoin:string,RestaurantsPriceRange2:string,Ambience:string,HasTV:string,Alcohol:string,GoodForMeal:string,DogsAllowed:string,RestaurantsTakeOut:string,NoiseLevel:string,RestaurantsAttire:string,RestaurantsDelivery:string,GoodForKids:string,ByAppointmentOnly:string,AcceptsInsurance:string,HairSpecializesIn:string,GoodForDancing:string,BestNights:string,Music:string,BYOB:string,CoatCheck:string,Smoking:string,DriveThru:string,BYOBCorkage:string,Corkage:string,RestaurantsCounterService:string>"

    }
    columns {
      name = "categories"
      type = "string"

    }
    columns {
      name = "hours"
      type = "struct<Monday:string,Tuesday:string,Wednesday:string,Thursday:string,Friday:string,Saturday:string,Sunday:string>"

    }

  }
}


resource "aws_glue_job" "job_name" {
  name         = "yelp_demo"
  description  = "demo job for glue"
  role_arn     = "arn:aws:iam::668102661106:role/GlueServiceRole"
  max_capacity = 0.0625
  max_retries  = 1
  timeout      = 60
  glue_version = "1.0"

  command {
    script_location = "s3://jj-datalake/scripts/hellow_yelp.py"
    python_version  = "3"
    name            = "pythonshell"
  }

  default_arguments = {
    "--env"            = "value",
    "--param1"         = "demo1",
    "--param2"         = "demo2"
    "--extra-py-files" = "s3://jj-datalake/libraries/dummy_package-0.1-py3-none-any.whl"
  }

  execution_property {
    max_concurrent_runs = 1
  }

  depends_on = [
    aws_s3_bucket.datalake
  ]

}