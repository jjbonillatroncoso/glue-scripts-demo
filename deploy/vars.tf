variable "prefix" {
  default = "demo-lakehouse"
}

variable "datalake_s3_bucket_name" {
  type = string
}

variable "query_results_s3_bucket_name" {
  type = string
}


variable "contact" {
  default = "jjbonillatroncoso@gmail.com"
}

variable "project" {
  default = "demo-lakehouse"
}


