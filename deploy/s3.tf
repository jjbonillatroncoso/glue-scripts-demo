resource "aws_s3_bucket" "athena-results" {
  bucket        = var.query_results_s3_bucket_name
  acl           = "private"
  force_destroy = true
}

resource "aws_s3_bucket" "datalake" {
  bucket        = var.datalake_s3_bucket_name
  acl           = "private"
  force_destroy = true
}

resource "aws_s3_bucket_object" "yelp_scripts" {
  for_each = fileset("templates/yelp_scripts/", "*")
  bucket   = aws_s3_bucket.datalake.id
  key      = "scripts/${each.value}"
  source   = "templates/yelp_scripts/${each.value}"
  etag     = filemd5("templates/yelp_scripts/${each.value}")
}