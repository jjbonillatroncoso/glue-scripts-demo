terraform {
  backend "s3" {
    bucket         = "demo-lakehouse-devops-tf-state"
    key            = "demo-lakehouse.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "demo-lakehouse-devops-tf-state-loc"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
data "aws_caller_identity" "current" {}