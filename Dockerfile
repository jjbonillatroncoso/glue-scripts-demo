FROM amazon/aws-glue-libs:glue_libs_1.0.0_image_01
RUN mkdir /home/scripts
RUN mkdir /home/jobs
COPY main /home/jobs
COPY scripts /home/scripts
RUN mkdir /home/libraries 
COPY custom_library /home/libraries/custom_library

WORKDIR /home/scripts